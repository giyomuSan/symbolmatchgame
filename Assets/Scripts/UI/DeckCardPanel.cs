﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using System;
using System.Linq;

namespace BJ
{
	public class DeckCardPanel : Panel
	{
		#region fields
		[SerializeField]
		private GameObject cardPrefab;
		[SerializeField]
		private Transform contents;
		[SerializeField]
		private Card[] cards;
		#endregion

		#region methods
		protected override void Init()
		{
			cards = GetComponentsInChildren<Card>();
			//SetCardDeck();
			base.Init ();
		}

		public override void Show()
		{
			base.Show();
		}

		public void SetCardDeck()
		{
			CardSerie serie = GameManager.Instance.GetCardDeck();

			if(serie != null)
			{
				CreateCard(serie.Deck.Count);
				SetStartSymbolInSlot(serie.Deck[0][0]);

				Card[] shuffled = ShuffleDeck();

				for(int i = 0; i < cards.Length; i++)
				{
					shuffled[i].InitializeCardSymbol(serie.Deck[i], i);
				}
			}
			else
			{
				Debug.LogError("Serie returned null value");
			}
		}

		private void CreateCard(int number)
		{
			cards = new Card[number];

			for(int i = 0; i < cards.Length; i++)
			{
				var instance = Instantiate<GameObject>(cardPrefab);
				instance.transform.SetParent(contents, false);

				cards[i] = instance.GetComponent<Card>();
			}
		}

		private void SetStartSymbolInSlot(CardData card)
		{
			SlotCardPanel panel = PanelManager.Instance.SlotCardPanel;

			if(panel)
			{
				panel.SetStartSymbol(card.Symbol, card.OverlayColor);
				panel.InitializeSlot();
			}
		}


		private Card[] ShuffleDeck()
		{
			if(DebugManager.Instance.ShowCardInResultOrder) return cards;

			Card[] shuffled = cards.OrderBy(c => Guid.NewGuid()).ToArray();
			return shuffled;

		}

		public override void Dismiss()
		{
			base.Dismiss();
		}
		#endregion
	}
}
