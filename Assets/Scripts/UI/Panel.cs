﻿using System.Collections;
using UnityEngine;

namespace BJ
{
	public abstract class Panel : MonoBehaviour 
	{
		#region fields
		public bool DismissOnAwake;
		#endregion

		#region methods
		private void Awake()
		{
			Init();
			if(DismissOnAwake) Dismiss();
		}

		protected virtual void Init(){}

		public virtual void Show()
		{
			if(gameObject.activeSelf != false) return;
			gameObject.SetActive(true);
		}

		public virtual void Dismiss()
		{
			if(gameObject.activeSelf == false) return;
			gameObject.SetActive(false);
		}
		#endregion
	}
}
