﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

namespace BJ
{

	public class SlotCardPanel : Panel
	{
		#region fields
		[SerializeField]
		private Image startSymbol;
		[SerializeField]
		private Slot[] slots;
		private int lastIndex;
		[SerializeField]
		private Sprite[] symbols;
		#endregion

		#region methods
		protected override void Init ()
		{
			//lastIndex = GameManager.Instance.GetCardSerieNumber();
			base.Init ();
		}

		public override void Show()
		{
			base.Show ();
		}

		public override void Dismiss ()
		{
			base.Dismiss ();
		}

		public void SetStartSymbol(CardSymbol symbol, Color32 color)
		{
			Sprite spr = GetSymbolSprite(symbol);
			Color32 col = color;

			startSymbol.sprite = spr;
			startSymbol.color = col;
		}

		public void InitializeSlot()
		{
			lastIndex = GameManager.Instance.GetCardSerieNumber();

			for(int i = 0; i < slots.Length; i++)
			{
				bool active = i < lastIndex ? true : false;
				slots[i].gameObject.SetActive(active);
			}

			slots[0].IsAvailable = true;
		}

		// TODO : not needed since we drag drop the card 
		private Sprite GetSymbolSprite(CardSymbol symbol)
		{
			switch(symbol)
			{
				case CardSymbol.Triangle:
					return symbols[0];
				case CardSymbol.Circle:
					return symbols[1];
				case CardSymbol.Square:
					return symbols[2];
				case CardSymbol.Cross:
					return symbols[3];
				default:
					return null;
			}
		}

		public Slot GetSlotAtIndex(int index)
		{
			if(index <= lastIndex)
			{
				return slots[index];
			}

			return null;
		}

		public void ResetSlot()
		{
			for(int i = 0; i < slots.Length; i++)
			{
				slots[i].ResetSlot();
			}
		}

		public bool IsLastSlotIndex(int currentIndex)
		{
			return lastIndex == currentIndex;
		}

		#endregion
	}
}

