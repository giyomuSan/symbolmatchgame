﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;

namespace BJ
{
	public class UserDisplayPanel : Panel
	{
		#region fields
		[SerializeField]
		private Text scoreDisplay;
		[SerializeField]
		private Button startPayButton;
		[SerializeField]
		private Button cardSerieToggle;

		#endregion

		#region methods
		protected override void Init()
		{
			startPayButton.onClick.AddListener(OnStartPlay);
			cardSerieToggle.onClick.AddListener(OnCardSerieTogglePressed);
			base.Init ();
		}

		public override void Show()
		{
			base.Show ();
		}
			
		public override void Dismiss()
		{
			base.Dismiss ();
		}

		private void OnStartPlay()
		{
			AudioManager.Instance.PlaySoundType(AudioManager.SoundType.StartPlay);
			GameManager.Instance.StartGame();
		}

		private void OnCardSerieTogglePressed()
		{
			CardSeries current = GameManager.Instance.SetCardSerieNumber();

			Text toggleText = cardSerieToggle.GetComponentInChildren<Text>();
			if(current == CardSeries.Standard)
			{
				toggleText.text = "4\nCards";
			}
			else
			{
				toggleText.text = "5\nCards";
			}
		}

		public void SetStartButtonState(bool state)
		{
			startPayButton.gameObject.SetActive(state);
		}

		public void SetCardSerieToggleState(bool state)
		{
			cardSerieToggle.gameObject.SetActive(state);
		}

		public void UpdateScore(int score)
		{
			scoreDisplay.text = score.ToString();
		}
		#endregion
	}

}

