﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;

public class UIDraggable : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler
{
	#region fields
	Vector2 startPosition;
	Transform startParent;
	public static GameObject CurrentDraggedObnject;
	#endregion

	#region drag handler implementation
	public void OnBeginDrag(PointerEventData eventData)
	{
		CurrentDraggedObnject = gameObject;
		startPosition = transform.position;
		startParent = transform.parent;
		GetComponent<CanvasGroup>().blocksRaycasts = false;
	}

	public void OnDrag(PointerEventData eventData)
	{
		transform.position = Input.mousePosition;
	}

	public void OnEndDrag(PointerEventData eventData)
	{
		CurrentDraggedObnject = null;

		if(transform.parent == startParent)
		{
			transform.position = startPosition;
			GetComponent<CanvasGroup>().blocksRaycasts = true;
		}
	}
	#endregion
}

