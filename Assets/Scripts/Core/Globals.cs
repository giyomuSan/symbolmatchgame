﻿using UnityEngine;
using System.Collections.Generic;

namespace BJ
{
	public enum CardSymbol
	{
		Triangle, 
		Circle,
		Square,
		Cross,
		Unknown
	}

	public enum SymbolFunction
	{
		Numerator,
		Denominator,
		Unknown
	}

	public enum CardSeries
	{
		Standard = 4,
		Advanced = 5
	}

	[System.Serializable]
	public class CardData
	{
		public SymbolFunction Function;
		public CardSymbol Symbol;
		public Color32 OverlayColor;
	}
		
	public class CardSerie
	{
		private List<CardData[]> deck;

		public List<CardData[]> Deck {get{return deck;}}

		public CardSerie()
		{
			deck = new List<CardData[]>();
		}

		public void AddCardData(CardData[] datas)
		{
			deck.Add(datas);
		}
	}
		
	public class DeckDataModel
	{
		private Dictionary<int, CardSerie> cardSerieCollection;

		private const string number = "number";
		private const string deck = "deck";

		private const string standardSerieFileName = "standard";
		private const string advancedSerieFileName = "advanced";

		public DeckDataModel(int serieNumber)
		{
			string fileName;
			if(serieNumber == 4)
				fileName = standardSerieFileName;
			else
				fileName = advancedSerieFileName;

			List<Dictionary<string, object>> datas = CSVReader.Read(fileName);
			//Debug.Log(fileName +", data Count = "+datas.Count);
		
			cardSerieCollection = new Dictionary<int, CardSerie>();
			CardData[] card = new CardData[2];


			for(int i = 0; i < datas.Count; i++) 
			{
				int serieNumberIndex = (int)datas[i][number];

				var param = SplitParameters(datas[i][deck]);

				CardSerie cardSerie = new CardSerie();

				int step = 6;
				int cardIndex = -1;

				// Note : each card represent a 6 string range data
				for(int j = 0; j < param.Length; j++)
				{
					if(j % step == 0)
					{
						cardIndex++;
					}

					int numIndex = 0;
					if(int.TryParse(param[j], out numIndex))
					{
						if(numIndex == 0)
						{
							int symbOffs = 1 + (step * cardIndex);
							int colorOffs = 2 + (step * cardIndex);

							var symbol = GetCardSymbol(param[symbOffs]);
							var color = GetColor(param[colorOffs]);

							card[numIndex] = new CardData
							{
								Function = SymbolFunction.Numerator,
								Symbol = symbol,
								OverlayColor = color
							};
						}
						else if(numIndex == 1)
						{
							int symbOffs = 4 + (step * cardIndex);
							int colorOffs = 5 + (step * cardIndex);

							var symbol = GetCardSymbol(param[symbOffs]);
							var color = GetColor(param[colorOffs]);

							card[numIndex] = new CardData
							{
								Function = SymbolFunction.Denominator,
								Symbol = symbol,
								OverlayColor = color
							};

							cardSerie.AddCardData(card);
							card = new CardData[2];
						}
					}
				}

				cardSerieCollection.Add(i, cardSerie);
			}
		}

		private string[] SplitParameters(object parameter)
		{
			//Debug.Log(parameter.ToString());
			string[] split = parameter.ToString().Split(',');

			return split;
		}

		private SymbolFunction GetSymbolFunction(string value)
		{
			switch(value)
			{
				case "0":
					return SymbolFunction.Numerator;
				case "1":
					return SymbolFunction.Denominator;
				default:
					return SymbolFunction.Unknown;
			}
		}

		private CardSymbol GetCardSymbol(string value)
		{
			switch(value)
			{
				case "triangle":
					return CardSymbol.Triangle;
				case "circle":
					return CardSymbol.Circle;
				case "square":
					return CardSymbol.Square;
				case "cross":
					return CardSymbol.Cross;
				default:
					return CardSymbol.Unknown;
			}
		}

		private Color32 GetColor(string value)
		{
			switch(value)
			{
				case "red":
					return new Color32(255,0,0,255);
				case "blue":
					return new Color32(0,0,255,255);
				case "green":
					return new Color32(0,255,0,255);
				case "pink":
					return new Color32(255,0,255,255);
				case "purple":
					return new Color32(125,0,255,255);
				case "brown":
					return new Color32(141,64,5,255);
				case "cyan":
					return new Color32(0,255,255,255);
				case "orange":
					return new Color32(255,125,0,255);
				default:
				 	return new Color32(255,255,255,255);
			}
		}

		public int GetCardSerieCount()
		{
			return cardSerieCollection.Count;
		}

		public CardSerie GetCardSerieAtIndex(int index)
		{
			if(cardSerieCollection.ContainsKey(index))
			{
				return cardSerieCollection[index];
			}
			else
			{
				return null;
			}
		}
	}
}
