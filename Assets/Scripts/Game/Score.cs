﻿using UnityEngine;
using System;
using System.Collections;

namespace BJ
{
	public class Score : MonoBehaviour
	{
		#region fields
		[SerializeField]
		private int totalScore;
		[SerializeField]
		private int currentScore;
		[SerializeField]
		private int successCardPoints;
		[SerializeField]
		private int failedCardPoints;
		[SerializeField]
		private int pointPerSecondBonus;
		#endregion

		#region methods
		public void UpdateScore(bool isFail)
		{
			if(currentScore == 0 && isFail) return;

			if(isFail) currentScore -= failedCardPoints;
			else currentScore += successCardPoints;
		}

		public void ResetScore()
		{
			currentScore = 0;
		}

		public int GetCalculadteScore(float time, float referenceTime)
		{
			int score = currentScore;

			ResetScore();

			int delta = Mathf.RoundToInt(referenceTime - time);
			//Debug.Log("time = " + time + " >> " + delta);
		
			score += delta * pointPerSecondBonus;

			if(score > 0) totalScore += score;

			return totalScore;
		}

		#endregion
	}
}

