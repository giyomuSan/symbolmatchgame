﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.Events;

namespace BJ
{
	public class Card : MonoBehaviour,IPointerClickHandler
	{
		#region fields
		[SerializeField]
		public Image Numerator;
		[SerializeField]
		public Image Denominator;
		[SerializeField]
		private Button flipBtn;
		[SerializeField]
		private Button selectBtn;
		[SerializeField]
		private CardData[] datas;
		[SerializeField]
		private CardData numeratorData;
		[SerializeField]
		private CardData denominatorData;
		[SerializeField]
		private Sprite[] symbols;
		[SerializeField]
		private int slotId;
		[SerializeField]
		private bool flipped;
		#endregion

		#region methods
		private void Start()
		{
			//flipBtn.onClick.AddListener(OnFlipButton);
			//selectBtn.onClick.AddListener(OnSelectButton);

			Numerator.preserveAspect = true;
			Denominator.preserveAspect = true;

			// TODO : this should be set from data files in final version
			//InitializeCardSymbol(datas);
		}

		public void OnPointerClick (PointerEventData eventData)
		{
			if(UIDraggable.CurrentDraggedObnject == gameObject) return;

			AudioManager.Instance.PlaySoundType(AudioManager.SoundType.CardFlip);
			FlipCard();
		}

		/*private void OnFlipButton()
		{
			AudioManager.Instance.PlaySoundType(AudioManager.SoundType.CardFlip);
			FlipCard();
		}*/

		private void FlipCard()
		{
			CardData tempNumerator = numeratorData;

			numeratorData = denominatorData;
			denominatorData = tempNumerator;

			SetNumerator();
			SetDenominator();

			flipped = !flipped;
		}

		public void OnCardDropped()
		{
			GameManager.Instance.SetCurrentActiveSlot(numeratorData, denominatorData, slotId, flipped);
		}

		private void OnSelectButton()
		{
			GameManager.Instance.SetCurrentActiveSlot(numeratorData, denominatorData, slotId, flipped);
			gameObject.SetActive(false);
		}

		public void InitializeCardSymbol(CardData[] datas, int slotId)
		{
			if(datas == null) return;

			flipped = false;

			this.datas = datas;
			this.slotId = slotId;

			for(int i = 0; i < datas.Length; i++)
			{
				if(datas[i].Function == SymbolFunction.Numerator)
				{
					numeratorData = datas[i];
				}
				else if(datas[i].Function == SymbolFunction.Denominator)
				{
					denominatorData = datas[i];
				}
			}

			SetNumerator();
			SetDenominator();

			if(!DebugManager.Instance.ShowCardInResultOrder)
			{
				if(Random.value > 0.5 ) FlipCard();
			}

			gameObject.SetActive(true);
		}

		private void SetNumerator()
		{
			Numerator.sprite = GetSprite((int)numeratorData.Symbol);
			Numerator.color = numeratorData.OverlayColor;
		}

		private void SetDenominator()
		{
			Denominator.sprite = GetSprite((int)denominatorData.Symbol);
			Denominator.color = denominatorData.OverlayColor;
		}

		private Sprite GetSprite(int index)
		{
			return symbols[index];
		}
		#endregion
	}
}
