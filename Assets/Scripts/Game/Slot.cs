﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;

namespace BJ
{
	public class Slot : MonoBehaviour,IDropHandler
	{
		#region fields

		public bool IsAvailable;

		[SerializeField]
		private GameObject droppedCard;
		[SerializeField]
		private Image cardFrame;
		[SerializeField]
		private Image numeratorImg;
		[SerializeField]
		private Image denominatorImg;
		[SerializeField]
		private Sprite[] symbols;
		#endregion

		#region drop handler implementation
		public void OnDrop(PointerEventData eventData)
		{
			if(!IsAvailable) return;

			droppedCard = UIDraggable.CurrentDraggedObnject;

			droppedCard.transform.position = transform.position;
			droppedCard.transform.SetParent(transform);

			droppedCard.GetComponent<Card>().OnCardDropped();

			IsAvailable = false;
		}
		#endregion

		#region methods
		void Start ()
		{
			numeratorImg.preserveAspect = true;
			denominatorImg.preserveAspect = true;
		}

		public void SetSlot(CardData numerator, CardData denominator)
		{
			cardFrame.color = new Color32(255,255,255,255);

			numeratorImg.sprite = GetSprite((int)numerator.Symbol);
			numeratorImg.color = numerator.OverlayColor;

			denominatorImg.sprite = GetSprite((int)denominator.Symbol);
			denominatorImg.color = denominator.OverlayColor;
		}

		public void ResetSlot()
		{
			/*cardFrame.color = new Color32(255,255,255,96);

			numeratorImg.sprite = null;
			numeratorImg.color = new Color32(255,255,255,0);

			denominatorImg.sprite = null;
			denominatorImg.color = new Color32(255,255,255,0);*/

			if(droppedCard != null)
			{
				Destroy(droppedCard);
			}

			IsAvailable = false;
		}

		private Sprite GetSprite(int index)
		{
			return symbols[index];
		}
		#endregion
	}
}

