﻿using UnityEngine;
using System.Collections;

namespace BJ
{
	public class Timer : MonoBehaviour
	{
		#region fields
		[SerializeField]
		private float referenceTime;
		[SerializeField]
		private float elapsedTime;
		private float lastTime;
		#endregion

		#region properties
		public float ReferenceTime{get{return referenceTime;}}
		public float LastTime{get{return lastTime;}}
		#endregion

		#region methods
		void Start()
		{
			StopTimer();	
		}

		public void StartTimer()
		{
			enabled = true;;
		}

		public void StopTimer()
		{
			enabled = false;
			lastTime = elapsedTime;
			elapsedTime = 0f;
		}

		public void Update()
		{
			elapsedTime += Time.unscaledDeltaTime;
		}
		#endregion
	}
}

