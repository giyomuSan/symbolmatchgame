﻿using UnityEngine;
using System.Collections;

namespace BJ
{
	public class PanelManager : MonoBehaviourSingleton<PanelManager>
	{
		#region fields
		public SlotCardPanel SlotCardPanel;
		public DeckCardPanel DeckCardPanel;
		public UserDisplayPanel UserDisplayPanel;
		#endregion
	}
}

