﻿using UnityEngine;

namespace BJ
{
	public class GameManager : MonoBehaviourSingleton<GameManager> 
	{
		#region fields
		[SerializeField]
		private int currentSlotIndex;
		[SerializeField]
		private int currentSerieIndex;
		private DeckDataModel deck;
		private DeckDataModel standardDeck;
		private DeckDataModel advancedDeck;
		[SerializeField]
		private CardSeries currentSerieNumber = CardSeries.Standard;
		private Timer timer;
		private Score score;
		#endregion

		#region methods
		private void Awake()
		{
			timer = GetComponent<Timer>();
			score = GetComponent<Score>();

			standardDeck = new DeckDataModel((int)CardSeries.Standard);
			advancedDeck = new DeckDataModel((int)CardSeries.Advanced);

			// default deck to standard
			deck = standardDeck;
		}

		public void StartGame()
		{
			PanelManager.Instance.DeckCardPanel.Show();
			PanelManager.Instance.DeckCardPanel.SetCardDeck();

			PanelManager.Instance.UserDisplayPanel.SetStartButtonState(false);
			PanelManager.Instance.UserDisplayPanel.SetCardSerieToggleState(false);
			timer.StartTimer();
		}
			
		public CardSeries SetCardSerieNumber()
		{
			currentSerieNumber = currentSerieNumber == CardSeries.Standard ? CardSeries.Advanced : CardSeries.Standard;

			switch(currentSerieNumber)
			{
				case CardSeries.Standard:
					deck = standardDeck;
					currentSerieIndex = RandomizeSerieIndex();
					break;
				case CardSeries.Advanced:
					deck = advancedDeck;
					currentSerieIndex = RandomizeSerieIndex();
					break;
			}

			return currentSerieNumber;
		}

		public int GetCardSerieNumber()
		{
			currentSerieIndex = RandomizeSerieIndex();
			return deck.GetCardSerieAtIndex(currentSerieIndex).Deck.Count;
		}

		private int RandomizeSerieIndex()
		{
			if(DebugManager.Instance.RandomizeSerieIndex == false) return 0;

			string seed = Time.time.ToString();
			System.Random pseudoRandom = new System.Random(seed.GetHashCode());

			int max = deck.GetCardSerieCount();
			return pseudoRandom.Next(0,max);
		}

		public CardSerie GetCardDeck()
		{
			return deck.GetCardSerieAtIndex(currentSerieIndex);
		}

		public void SetCurrentActiveSlot(CardData numerator, CardData denominator, int slotId, bool flipped)
		{
			// TODO : check for validation
			bool isFail;
			if(!IsCardValid(slotId, flipped))
			{
				AudioManager.Instance.PlaySoundType(AudioManager.SoundType.CardFail);
				isFail = true;
			}
			else
			{
				AudioManager.Instance.PlaySoundType(AudioManager.SoundType.CardSuccess);
				isFail = false;
			}

			score.UpdateScore(isFail);

			// set slot card
			var panel = PanelManager.Instance.SlotCardPanel;

			if(panel)
			{
				currentSlotIndex++;

				if(panel.IsLastSlotIndex(currentSlotIndex))
				{
					ResetBoard();
				}
				else
				{
					//panel.GetSlotAtIndex(currentSlotIndex).SetSlot(numerator, denominator);
					panel.GetSlotAtIndex(currentSlotIndex).IsAvailable = true;
				}
			}
		}

		private bool IsCardValid(int slotId, bool flipped)
		{
			return slotId == currentSlotIndex && !flipped;
		}

		public void CheckScore()
		{
			float time = timer.LastTime;
			float refTime = timer.ReferenceTime;
			int currScore = score.GetCalculadteScore(time, refTime);

			PanelManager.Instance.UserDisplayPanel.UpdateScore(currScore);
		}
			
		public void ResetBoard()
		{
			// TODO : will be used to clean the screen and relaod a new set of cards.
			//Debug.Log("BOARD RESET");

			timer.StopTimer();

			CheckScore();

			currentSlotIndex = 0;
			currentSerieIndex++;

			if(currentSerieIndex >= deck.GetCardSerieCount())
			{
				currentSerieIndex = 0;
			}

			// TODO :  get back the card put in slot and reput them into the desk component.
			PanelManager.Instance.SlotCardPanel.ResetSlot();
			PanelManager.Instance.UserDisplayPanel.SetStartButtonState(true);
			PanelManager.Instance.UserDisplayPanel.SetCardSerieToggleState(true);
		}
		#endregion
	}
}
