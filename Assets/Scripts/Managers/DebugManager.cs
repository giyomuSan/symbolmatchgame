﻿using UnityEngine;
using System.Collections;

namespace BJ
{
	public class DebugManager : MonoBehaviourSingleton<DebugManager>
	{
		#region fields
		public bool ShowCardInResultOrder;
		public bool RandomizeSerieIndex;
		#endregion

		#region methods
		private void Awake()
		{
			#if !UNITY_EDITOR
			ShowCardInResultOrder = false;
			RandomizeSerieIndex = true;
			#endif
		}
		#endregion
	}
}

