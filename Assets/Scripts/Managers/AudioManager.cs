﻿using UnityEngine;
using System.Collections;

namespace BJ
{
	public class AudioManager : MonoBehaviourSingleton<AudioManager>
	{
		#region enum
		public enum SoundType
		{
			CardFail,
			CardSuccess,
			CardDrop,
			CardFlip,
			StartPlay
		}
		#endregion

		#region class
		[System.Serializable]
		public class SoundFx
		{
			public SoundType SoundType;
			public AudioClip Clip;
		}
		#endregion

		#region fields
		[SerializeField]
		private AudioSource sfx;
		[SerializeField]
		private SoundFx[] sounds;
		#endregion

		#region methods
		public void PlaySoundType(SoundType type)
		{
			var clip = GetAudioClip(type);

			if(clip != null)
			{
				sfx.PlayOneShot(clip);
			}
		}

		private AudioClip GetAudioClip(SoundType type)
		{
			for(int i = 0; i < sounds.Length; i++)
			{
				if(sounds[i].SoundType == type)
				{
					return sounds[i].Clip;
				}
			}

			return null;
		}
		#endregion
	}
}

